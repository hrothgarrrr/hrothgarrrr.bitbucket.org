#-----------------------------------------------------------------------------
# BUILD.py
# Hrothgar, created 27 Feb 2014
#-----------------------------------------------------------------------------
from zookeeper import *
zoo = Zoo()


#-----------------------------------------------------------------------------
# Eigenvalue problems
#-----------------------------------------------------------------------------

zoo.add(
    name        = "Sturm&ndash;Liouville problem",
    date        = "27 June 2014",
    slug        = "sturmLiouville",
    type        = ["evp"],
    domain      = "[0,1]",
    equations   = [ "-(p(x)u')' + q(x)u = \\lambda w(x)u",
                    "\\alpha_1 u(0) + \\alpha_2 u'(0) = 0",
                    "\\beta_1 u(1) + \\beta_2 u'(1) = 0" ],
    schema      = [
        [ [Blk("-D M_{p(x)} D + M_{q(x)}")],
            [Row("\\alpha_1\\mathbf{e}_0^T + \\alpha_2\\mathbf{e}_0^T D")],
            [Row("\\beta_1\\mathbf{e}_1^T + \\beta_2\\mathbf{e}_1^T D")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Blk("M_{w(x)}")],
            [Row.bold(0)],
            [Row.bold(0)] ],
        [ [Col.bold("u")] ]
    ],
    size        = 7
    )

zoo.add(
    name        = "Spectral parameter in boundary conditions",
    date        = "27 June 2014",
    slug        = "spectralParameterInBC",
    type        = ["evp"],
    domain      = "[0,1]",
    equations   = [ "-u'' + q(x)u = \\lambda^2 u",
                    "(\\alpha_0 + \\alpha_1 \\lambda + \\alpha_2 \\lambda^2) u(0) + u'(0) = 0",
                    "(\\beta_0 + \\beta_1 \\lambda + \\beta_2 \\lambda^2) u(1) + u'(1) = 0" ],
    schema      = [
        [ [Blk("-D^2 + M_{q(x)}"), Col.bold(0)],
            [Row("(u,\\lambda) \\mapsto \\alpha(\\lambda) u(0) + u'(0)", span=2, linear=False)],
            [Row("(u,\\lambda) \\mapsto \\beta(\\lambda) u(1) + u'(1)", span=2, linear=False)] ],
        [ [Col.bold("u")], [Scl("\\lambda")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda^2", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 8,
    refs        = [{"url": "http://link.springer.com/article/10.1007%%2Fs11202-999-0008-5",
                    "text": """Kerimov, N. B., and Kh R. Mamedov. "On one boundary value problem with a spectral parameter in the boundary conditions." <em>Siberian Mathematical Journal</em> 40, no. 2 (1999): 281-290."""}]
    )

zoo.add(
    name        = "Advection-diffusion",
    slug        = "advectionDiffusionEVP",
    type        = ["evp"],
    domain      = "[0,10]",
    equations   = [ "u'' + u' = \\lambda u",
                    "u(0) = u(10) = 0"],
    schema      = [
        [ [Blk("D^2 + D")],
            [Row.e(0)],
            [Row.e(10)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 6,
    )

zoo.add(
    name        = "Airy operator",
    slug        = "airyEVP",
    type        = ["evp"],
    domain      = "[-1,1]",
    equations   = [ "\\varepsilon u'' + x u = \\lambda u",
                    "u(-1) = u(1) = 0"],
    schema      = [
        [ [Blk("\\varepsilon D^2 + M_{x}")],
            [Row.e(-1)],
            [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 6,
    )


# Outdated.
zoo.add(
    hidden      = True,
    name        = "Cross of eigenvalues",
    slug        = "cross",
    type        = ["evp"],
    domain      = "[0,2\\pi]",
    equations   = [ "-u'' = \\lambda (u+u')",
                    "u(0) = u(2\\pi) = 0"],
    schema      = [
        [ [Blk("-D^2")],
            [Row.e(0)],
            [Row.e("2\\pi")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Blk("\\lambda (D+I)")],
            [Row.bold(0)],
            [Row.bold(0)] ],
        [ [Col.bold("u")] ]
    ],
    size        = 6,
    )


zoo.add(
    name        = "Davies' complex harmonic oscillator",
    slug        = "davies",
    type        = ["evp"],
    domain      = "[-8,8]",
    equations   = [ "-u'' + i x^2 u = \\lambda u",
                    "u(-8) = u(8) = 0"],
    schema      = [
        [ [Blk("-D^2 + M_{ix^2}")],
            [Row.e(-8)],
            [Row.e(8)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 6,
    )


zoo.add(
    name        = "Second derivative operator",
    slug        = "derivative",
    type        = ["evp"],
    domain      = "[-\\pi,\\pi]",
    equations   = [ "-u'' = \\lambda u",
                    "u(-\\pi) = u(\\pi)",
                    "u'(-\\pi) = u'(\\pi)"],
    schema      = [
        [ [Blk("-D^2")],
            [Row("\\mathbf{e}_{\\pi}^T - \\mathbf{e}_{-\\pi}^T")],
            [Row("( \\mathbf{e}_{\\pi}^T - \\mathbf{e}_{-\\pi}^T ) D")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 6,
    )


zoo.add(
    name        = "Double well Schrodinger",
    slug        = "doubleWell",
    type        = ["evp"],
    domain      = "[-5,5]",
    equations   = [ "-h^2 u'' + a(\\sign(x-b)-\\sign(x-c)) u = \\lambda u",
                    "u(-5) = u(5) = 0"],
    schema      = [
        [ [Blk("-h D^2 \\\\\\quad + M_{a(\\sign(x-b)-\\sign(x-c))}")],
            [Row.e(-5)],
            [Row.e(5)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 7,
    )


zoo.add(
    name        = "Harmonic oscillator",
    slug        = "harmonic",
    type        = ["evp"],
    domain      = "[-8,8]",
    equations   = [ "-u'' + x^2 u = \\lambda u",
                    "u(-8) = u(8) = 0"],
    schema      = [
        [ [Blk("-D^2 + M_{x^2}")],
            [Row.e(-8)],
            [Row.e(8)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 6,
    )


zoo.add(
    name        = "Harmonic oscillator as 1st-order system",
    slug        = "harmonicSystem",
    type        = ["evp"],
    domain      = "[0,\\pi]",
    equations   = [ "u' = \\lambda v",
                    "-v' = \\lambda u",
                    "u(0) = u(\\pi) = 0"],
    schema      = [
        [ [Blk(0), Blk("-D")],
            [Blk("D"), Blk(0)],
            [Row.e(0), Row.bold(0)],
            [Row.e("\\pi"), Row.bold(0)] ],
        [ [Col.bold("u")],
            [Col.bold("v")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Col.bold("v")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 6,
    )


zoo.add(
    name        = "Mathieu equation",
    slug        = "mathieu",
    type        = ["evp"],
    domain      = "[-\\pi,\\pi]",
    equations   = [ "-u'' + 5\\cos(2x) u = \\lambda u",
                    "u(-\\pi) = u(\\pi)",
                    "u'(-\\pi) = u'(\\pi)"],
    schema      = [
        [ [Blk("-D^2 + M_{5\\cos(2x)}")],
            [Row("\\mathbf{e}_{\\pi}^T - \\mathbf{e}_{-\\pi}^T")],
            [Row("( \\mathbf{e}_{\\pi}^T - \\mathbf{e}_{-\\pi}^T ) D")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 6,
    )


zoo.add(
    name        = "Orr-Sommerfeld operator",
    slug        = "orrSommerfeld",
    type        = ["evp"],
    domain      = "[-1,1]",
    equations   = [ "\\alpha^{-1}(u''''-2u''+u) - 2\\i u - \\i(1-x^2)(u''-u) = \\lambda (u''-u)",
                    "u(-1) = u(1) = 0",
                    "u'(-1) = u'(1) = 0"],
    schema      = [
        [ [Blk("\\alpha^{-1}(D^4-2 D^2+I) - 2\\i I \\\\\\quad - M_{\\i(1-x^2)}(D^2-I)")],
            [Row.e(-1)],
            [Row.e(1)],
            [Row.eD(-1)],
            [Row.eD(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Blk("D^2 - I")],
            [Row.bold(0)],
            [Row.bold(0)],
            [Row.bold(0)],
            [Row.bold(0)] ],
        [ [Col.bold("u")] ]
    ],
    size        = 8,
    )


zoo.add(
    name        = "Time-independent Schr&#246;dinger equation",
    slug        = "schrodinger",
    type        = ["evp"],
    domain      = "[-3,3]",
    equations   = [ "L u \\equiv -h^2 u'' + v u = \\lambda u",
                    "u(-3) = u(3) = 0"],
    schema      = [
        [ [Blk("-h^2 D^2 + M_v")],
            [Row.e(-3)],
            [Row.e(3)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 6,
    )


zoo.add(
    name        = "System with variable coefficients",
    slug        = "systemVariableCoeffs",
    type        = ["evp"],
    domain      = "[-2,2]",
    equations   = [ "u'' + xu + v = \\lambda u",
                    "v'' + \\sin(x) u = \\lambda v",
                    "u(-2) = u(2) = 0",
                    "v(-2) = v(2) = 0"],
    schema      = [
        [ [Blk("D^2 + M_x"), Blk("I")],
            [Blk("M_{\\sin(x)}"), Blk("D^2")],
            [Row.e(-2), Row.bold(0)],
            [Row.e(2), Row.bold(0)],
            [Row.bold(0), Row.e(-2)],
            [Row.bold(0), Row.e(2)] ],
        [ [Col.bold("u")], [Col.bold("v")] ],
        [ [Sym("=")] ],
        [ [Sym("\\lambda", padded=False)] ],
        [ [Col.bold("u")],
            [Col.bold("v")],
            [Scl(0)],
            [Scl(0)],
            [Scl(0)],
            [Scl(0)] ]
    ],
    size        = 8,
    )



#-----------------------------------------------------------------------------
# PDEs / partial differential equations
#-----------------------------------------------------------------------------

zoo.add(
    # hidden      = True,
    name        = "Heat equation",
    date        = "27 June 2014",
    slug        = "heat",
    type        = ["pde"],
    domain      = "(x,t) \\in [-1,1] \\times [0,\\tau]",
    equations   = [ "u_t = u_{xx}",
                    "u(x,0) = u_0(x)",
                    "u_x(-1,t) = u_x(1,t) = 0"],
    schema      = [
        [ [Blk("\\partial_t - \\partial_{xx}")],
            [Row("\\mathbf{e}_{(-1,\\cdot)}^T \\partial_x")],
            [Row("\\mathbf{e}_{(1,\\cdot)}^T \\partial_x")],
            [Row("\\mathbf{e}_{(\\cdot,0)}^T")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(0)], [Scl("u_0")] ]
    ],
    size        = 7,
    )


zoo.add(
    # hidden      = True,
    name        = "Coupled PDE boundary value problem",
    date        = "27 June 2014",
    slug        = "coupledPdeBvp",
    type        = ["pde"],
    domain      = "(x,t) \\in [-1,1] \\times [0, T]",
    equations   = [ "u_t = u_{xx} - v",
                    "v_{xx} - u = 0",
                    "u(-1,t) = u(1,t) = 1",
                    "v(-1,t) = v(1,t) = 1"],
    schema      = [
        [ [Blk("\\partial_t - \\partial_{xx}"), Blk("I")],
            [Blk("-I"), Blk("\\partial_{xx}")],
            [Row.e("(-1,\\cdot)"), Row.bold(0)],
            [Row.e("(1,\\cdot)"), Row.bold(0)],
            [Row.bold(0), Row.e("(-1,\\cdot)")],
            [Row.bold(0), Row.e("(1,\\cdot)")] ],
        [ [Col.bold("u")], [Col.bold("v")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Col.bold(0)],
            [Scl(1)], [Scl(1)], [Scl(1)], [Scl(1)] ]
    ],
    size        = 10,
    notes       =  "There is more to think about with this one! E.g. split [u; v] into a 2x2 block [u,0;v,0] and see what happens with the boundary conditions....",
    )


zoo.add(
    # hidden      = True,
    name        = "Burgers' equation (viscous)",
    date        = "27 June 2014",
    slug        = "burgersViscous",
    type        = ["pde"],
    domain      = "(x,t) \\in [-1,1] \\times [0, T]",
    equations   = [ "u_t + u u_x = \\varepsilon u_{xx}",
                    "u(-1,t) = u(1,t)",
                    "u_x(-1,t) = u_x(1,t)"],
    schema      = [
        [ [Blk("N : u \\mapsto u_t + u u_x - \\varepsilon u_{xx}", linear=False)],
            [Row("\\mathbf{e}_{(1,\\cdot)}^T - \\mathbf{e}_{(-1,\\cdot)}^T")],
            [Row("( \\mathbf{e}_{(1,\\cdot)}^T - \\mathbf{e}_{(-1,\\cdot)}^T ) \\partial_x")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(0)] ]
    ],
    size        = 7,
    refs        = [{"url": "http://onlinelibrary.wiley.com/doi/10.1002/cpa.3160030302/abstract",
                    "text": """Hopf, Eberhard. "The partial differential equation $ u_t+uu_x=\mu u_{xx}. $" Communications on Pure and Applied mathematics 3, no. 3 (1950): 201-230."""}],
    notes       = "Take a look at the linearization by Cole--Hopf transformation.",
    )



#-----------------------------------------------------------------------------
# IVP examples
#-----------------------------------------------------------------------------

zoo.add(
    name      = "Bacterial respiration",
    date      = "27 June 2014",
    slug      = "bacterialRespiration",
    type      = ["ivp"],
    domain    = "[0,5]",
    equations = [ "u' = \\beta - u - \\frac{uv}{1 + \\gamma u^2}",
                  "v' = \\alpha - \\frac{uv}{1 + \\gamma u^2}",
                  "u(0) = 2",
                  "v(0) = 1"],
    schema    = [
        [ [Blk("N_1 : (u,v) \\mapsto \\\\\\quad u' - \\beta + u + \\frac{uv}{1 + \\gamma u^2}", w=2, linear=False)],
            [Blk("N_2 : (u,v) \\mapsto \\\\\\quad v' - \\alpha + \\frac{uv}{1 + \\gamma u^2}", w=2, linear=False)],
            [Row.e(0), Row.bold(0)],
            [Row.bold(0), Row.e(0)] ],
        [ [Col.bold("u")], [Col.bold("v")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Col.bold(0)], [Scl(2)], [Scl(1)] ]
    ],
    size      = 8,
    refs      = [{"url": "http://www.amazon.co.uk/Nonlinear-Dynamics-Chaos-Applications-nonlinearity/dp/0738204536",
                  "text": "Strogatz, Steven H. <em>Nonlinear dynamics and chaos: with applications to physics, biology and chemistry.</em> Perseus publishing, 2001: p288."}]
    )


zoo.add(
    name      = "R&ouml;ssler attractor",
    date      = "26 June 2014",
    slug      = "rossler",
    type      = ["ivp"],
    domain    = "[0,10]",
    equations = [ "u' = -v - w",
                  "v' = u + \\alpha v",
                  "w' = \\beta + w(u-\\gamma)",
                  "u(0) = 5",
                  "v(0) = 0",
                  "w(0) = 0"
                ],
    schema    = [
        [ [Blk("D"), Blk("I"), Blk("I")],
            [Blk("I"), Blk("D + \\alpha I"), Blk("0")],
            [Blk("N : (u,v,w) \\mapsto \\beta + w(u-\\gamma)", w=3, linear=False)],
            [Row.e(0), Row.bold(0), Row.bold(0)],
            [Row.bold(0), Row.e(0), Row.bold(0)],
            [Row.bold(0), Row.bold(0), Row.e(0)]
        ],
        [ [Col.bold("u")], [Col.bold("v")], [Col.bold("w")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Col.bold(0)], [Col.bold(0)],
            [Scl(5)], [Scl(0)], [Scl(0)] ],
    ],
    size      = 12,
    )


zoo.add(
    name      = "Blow-up initial value problem",
    slug      = "blowUpIvp",
    type      = ["ivp"],
    domain    = "[0,0.99]",
    equations = [ "u' = u^2",
                  "u(0) = 1"],
    schema    = [
        [ [Blk("N : u \\mapsto u' - u^2", linear=False)], [Row.e(0)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(1)] ]
    ],
    size      = 5.2,
    )


zoo.add(
    name      = "Lane-Emden equation",
    slug      = "laneEmden",
    type      = ["ivp"],
    domain    = "[0,10]",
    equations = [ "x u'' +2u'+x u^2 = 0",
                  "u(0) = 1",
                  "u'(0) = 0"],
    schema    = [
        [ [Blk("N : u \\mapsto x u'' +2u'+x u^2", linear=False)],
            [Row.e(0)], [Row.eD(0)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(1)], [Scl(0)] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Linear sawtooth",
    slug      = "linearSawtooth",
    type      = ["ivp"],
    domain    = "[0,8]",
    equations = [ "u' = \\sign(\\sin x^2) - \\tfrac12 u",
                  "u(0) = 0"],
    schema    = [
        [ [Blk("D + \\tfrac12 I")],
            [Row.e(0)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold("f")], [Scl(0)] ]
    ],
    size      = 6,
    )


zoo.add(
    name      = "Linear wave equation",
    slug      = "linearWave",
    type      = ["ivp"],
    domain    = "[0,10\\pi]",
    equations = [ "u'' = -u",
                  "u(0) = 2",
                  "u'(0) = 0"],
    schema    = [
        [ [Blk("D^2 + I")],
            [Row.e(0)], [Row.eD(0)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(2)], [Scl(0)] ]
    ],
    size      = 6,
    )


zoo.add(
    name      = "Lorenz equations",
    slug      = "lorenz",
    type      = ["ivp"],
    domain    = "[0,1]",
    equations = [ "u' = \\sigma(y - u)",
                  "v' = u(\\rho - z) - v",
                  "w' = uv - \\beta w",
                  "u(0) = 0",
                  "v(0) = 1",
                  "w(0) = 1"],
    schema    = [
        [ [Blk("D + \\sigma I"), Blk("-\\sigma I"), Blk("0")],
            [Blk("N_1 : (u,v,w) \\mapsto v' - u(\\rho-z) + v", span=3, linear=False)],
            [Blk("N_2 : (u,v,w) \\mapsto w' - uv + \\beta w", span=3, linear=False)],
            [Row.e(0), Row.bold(0), Row.bold(0)],
            [Row.bold(0), Row.e(0), Row.bold(0)],
            [Row.bold(0), Row.bold(0), Row.e(0)]
        ],
        [ [Col.bold("u")], [Col.bold("v")], [Col.bold("w")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Col.bold(0)], [Col.bold(0)],
            [Scl(0)], [Scl(1)], [Scl(1)] ],
    ],
    # schema    = [
    #     [ [Blk("0"), Blk("D + \\sigma I"), Blk("- \\sigma I")],
    #         [Blk("N_1 : (z,x) \\mapsto \\\\\\quad x(z-\\rho)", w=2, linear=False), Blk("D + I")],
    #         [Blk("D + \\beta I"), Blk("N_2 : (x,y) \\mapsto -xy", w=2, linear=False)],
    #         [Row.bold(0), Row.e(0), Row.bold(0)],
    #         [Row.bold(0), Row.bold(0), Row.e(0)],
    #         [Row.e(1), Row.bold(0), Row.bold(0)]
    #     ],
    #     [ [Col.bold("z")], [Col.bold("x")], [Col.bold("y")] ],
    #     [ [Sym("=")] ],
    #     [ [Col.bold(0)], [Col.bold(0)], [Col.bold(0)],
    #         [Scl(0)], [Scl(1)], [Scl(1)] ],
    # ],
    size      = 12,
    )


zoo.add(
    name      = "Lotka-Volterra predator-prey",
    slug      = "lotkaVolterra",
    type      = ["ivp"],
    domain    = "[0,18]",
    equations = [ "u' = u - uv",
                  "v' = -v + uv",
                  "u(0) = \\alpha",
                  "v(0) = \\beta"],
    schema    = [
        [ [Blk("N_1 : (u,v) \\mapsto u' - u + uv", w=2, linear=False)],
            [Blk("N_2 : (u,v) \\mapsto v' + v - uv", w=2, linear=False)],
            [Row.e(0), Row.bold(0)],
            [Row.bold(0), Row.e(0)] ],
        [ [Col.bold("u")], [Col.bold("v")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Col.bold(0)], [Scl("\\alpha")], [Scl("\\beta")] ]
    ],
    size      = 8,
    )


zoo.add(
    name      = "Parabolic cylinder equation",
    date      = "26 June 2014",
    slug      = "parabolicCylinder",
    type      = ["ivp"],
    domain    = "[-8,8]",
    equations = [ "u'' - (\\tfrac14 x^2 + a)u = 0",
                  "u(0) = 0",
                  "u'(0) = -1"
                ],
    schema    = [
        [ [Blk("D^2 - M_{x^2/4 + a}")], [Row.e(0)], [Row.eD(0)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(-1)] ],
    ],
    size      = 7,
    refs      = [{"url":  "http://dlmf.nist.gov/12.2",
                  "text": "NIST Digital Library of Mathematical Functions &sect;12.2.<br/><tt>http://dlmf.nist.gov/12.2</tt>."}]
    )


zoo.add(
    hidden    = True,
    name      = "Struve's equation",
    date      = "26 June 2014",
    slug      = "struve",
    type      = ["ivp"],
    domain    = "[0,12]",
    equations = [ "u'' + \\tfrac1x u' + (1-\\tfrac{\\nu^2}{x^2})u = \\frac{(x/2)^{\\nu-1}}{\\sqrt{\\pi}\\Gamma(\\nu+1/2)}",
                  "u(0) = 0",
                  "u'(0) = 1"
                ],
    schema    = [
        [ [Blk("D^2 + M_{1/x}D + M_{1-\\nu^2/x^2}")], [Row.e(0)], [Row.eD(0)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold("f")], [Scl(0)], [Scl(1)] ],
    ],
    size      = 8,
    refs      = [{"url":  "http://dlmf.nist.gov/11.2",
                  "text": "NIST Digital Library of Mathematical Functions &sect;11.2.<br/><tt>http://dlmf.nist.gov/11.2</tt>."}]
    )


zoo.add(
    name      = "Resonance",
    slug      = "resonance",
    type      = ["ivp"],
    domain    = "[0,20]",
    equations = [ "u'' + 25u = 10 \\cos(5t)",
                  "u(0) = 1",
                  "u'(0) = 0"],
    schema    = [
        [ [Blk("D^2 + 25 I")],
            [Row.e(0)], [Row.eD(0)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold("f")], [Scl(1)], [Scl(0)] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Van der Pol equation",
    slug      = "vanDerPol",
    type      = ["ivp"],
    domain    = "[0,15]",
    equations = [ "u'' - (1-u^2) u' + u = 0",
                  "u(0) = 2",
                  "u'(0) = 0"],
    schema    = [
        [ [Blk("N : u \\mapsto u'' - (1-u^2) u' + u", linear=False)],
            [Row.e(0)], [Row.eD(0)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(2)], [Scl(0)] ]
    ],
    size      = 8,
    )



#-----------------------------------------------------------------------------
# BVP examples
#-----------------------------------------------------------------------------

zoo.add(
    name      = "Bratu problem",
    date      = "27 June 2014",
    slug      = "bratu",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "u'' + \\lambda e^u = 0",
                  "u(0) = u(1) = 0"
                ],
    schema    = [
        [ [Blk("N : (u,\\lambda) \\mapsto u'' + \\lambda e^u", span=2, linear=False)],
            [Row.e(0), Scl(0)],
            [Row.e(1), Scl(0)] ],
        [ [Col.bold("u")], [Scl("\\lambda")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(0)] ],
    ],
    size      = 8,
    refs      = [{"url": "http://faculty.oxy.edu/ron/research/bratu/bratu.pdf",
                  "text": "http://faculty.oxy.edu/ron/research/bratu/bratu.pdf"}],
    notes     = """Need to work out the rest of this."""
    )


zoo.add(
    name      = "Cantilevered beam",
    date      = "26 June 2014",
    slug      = "cantileveredBeam",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "u'''' = -1",
                  "u(0) = u'(0) = 0",
                  "u''(1) = u'''(1) = 0"
                ],
    schema    = [
        [ [Blk("D^4")], [Row.e(0)], [Row.eD(0)], [Row.eD(1, order=2)], [Row.eD(1, order=3)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold("f")], [Scl(0)], [Scl(0)], [Scl(0)], [Scl(0)] ],
    ],
    size      = 8
    )



zoo.add(
    name      = "Advection-diffusion equation",
    slug      = "advectionDiffusion",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "\\varepsilon u'' + u' + u = 0",
                  "u(0) = 0",
                  "u(1) = 1"
                ],
    schema    = [
        [ [Blk("\\varepsilon D^2 + D + I")], [Row.e(0)], [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(1)] ],
    ],
    size      = 6,
    )


zoo.add(
    name      = "Advection-diffusion with interior point conditions",
    slug      = "advectionDiffusionInteriorPoints",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "\\varepsilon u'' + u' + u = 0",
                  "u'(0.5) = 2",
                  "\\int_0^{0.1} u(x)dx = 0"],
    schema    = [
        [ [Blk("\\varepsilon D^2 + D + I")],
            [Row.eD(0.5)], [Row("\\mathbf{s}^T M_{\\mathbf{1}_{[0,0.1]}}")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(2)], [Scl(0)] ]
    ],
    size      = 6,
    )


zoo.add(
    name      = "Advection-diffusion with jump conditions",
    slug      = "advectionDiffusionJumps",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "\\varepsilon u'' + u' + u = 0",
                  "u(0) = 0",
                  "u'(1) = -1",
                  "u_+(0.5) - u_-(0.5) = -1",
                  "u'_+(0.5) - u'_-(0.5) = 25"],
    schema    = [
        [ [Blk("\\varepsilon D^2 + D + I")],
            [Row.e(0)], [Row.eD(1)],
            [Row.j(0.5)], [Row.jD(0.5)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl("-1")], [Scl("-1")], [Scl("25")] ]
    ],
    size      = 8,
    )


zoo.add(
    name      = "Advection-diffusion with discontinuous coefficient",
    slug      = "advectionDiffusionDiscontinuous",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "\\varepsilon u'' + \\sign(x-\\tfrac12) u' + u = 0",
                  "u(0) = 0.5",
                  "u(1) = 1"],
    schema    = [
        [ [Blk("N : u \\mapsto \\varepsilon u'' \\\\\\quad + \\sign(x-\\tfrac12) u' + u")],
            [Row.e(0)], [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl("0.5")], [Scl(1)] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Airy equation",
    slug      = "airy",
    type      = ["bvp"],
    domain    = "[-5,5]",
    equations = [ "\\varepsilon u'' - xu = 1",
                  "u(-5) = 0",
                  "u(5) = 0"],
    schema    = [
        [ [Blk("\\varepsilon D^2 - M_x")], [Row.e(-5)], [Row.e(5)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(1)], [Scl(0)], [Scl(0)] ]
    ],
    size      = 6,
    )


zoo.add(
    name      = "Bessel equation",
    slug      = "bessel",
    type      = ["bvp"],
    domain    = "[0,30]",
    equations = [ "x^2 u'' + x u' + (x^2-3^2) u = 0",
                  "u(0) = 0",
                  "u(30) = 1"],
    schema    = [
        [ [Blk("M_{x^2} D^2 + M_x D \\\\\\quad + M_{x^2-3^2}")],
            [Row.e(0)], [Row.e(30)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(1)] ]
    ],
    size      = 6,
    )


zoo.add(
    name      = "Carrier equation",
    slug      = "carrier",
    type      = ["bvp"],
    domain    = "[-1,1]",
    equations = [ "\\varepsilon u'' + 2(1-x^2)u + u^2 = 1",
                  "u(-1) = u(1) = 0"],
    schema    = [
        [ [Blk("N : u\\mapsto \\varepsilon u'' \\\\\\quad + 2(1-x^2)u' + u^2", linear=False)],
            [Row.e(-1)], [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(1)], [Scl(0)], [Scl(0)] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Coupled system with sin(v) and cos(u)",
    slug      = "coupledSystemSinCos",
    type      = ["bvp"],
    domain    = "[-1,1]",
    equations = [ "u'' - \\sin(v) = 0",
                  "v'' + \\cos(u) = 0",
                  "u(-1) = 1",
                  "u'(1) = 0",
                  "v(1) = 0",
                  "v'(-1) = 0"],
    schema    = [
        [ [Blk("N_1 : (u,v) \mapsto u'' - \\sin(v)", span=2, linear=False)],
            [Blk("N_2 : (u,v) \mapsto v'' + \\cos(u)", span=2, linear=False)],
            [Row.e(-1), Row.bold(0)],
            [Row.eD(1), Row.bold(0)],
            [Row.bold(0), Row.e(1)],
            [Row.bold(0), Row.eD(-1)] ],
        [ [Col.bold("u")], [Col.bold("v")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Col.bold(0)],
            [Scl(1)], [Scl(0)], [Scl(0)], [Scl(0)] ]
    ],
    size      = 12,
    )


zoo.add(
    name      = "Fisher-KPP equation",
    slug      = "fisherKPP",
    type      = ["bvp"],
    domain    = "[-4,4]",
    equations = [ "u'' + u - u^2 = 0",
                  "u(-4) = 1",
                  "u(4) = 0"],
    schema    = [
        [ [Blk("N : u \\mapsto u'' + u - u^2", linear=False)],
            [Row.e(-4)], [Row.e(4)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(1)], [Scl(0)] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Nonlinear fourth-order equation",
    slug      = "fourthOrder",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "u'''' = u' u'' - u u'''",
                  "u(0) = 0",
                  "u'(0) = 0",
                  "u(1) = 1",
                  "u'(1) = -5"],
    schema    = [
        [ [Blk("N : u \\mapsto u'''' - u' u'' + u u'''", linear=False)],
            [Row.e(0)],
            [Row.eD(0)],
            [Row.e(1)],
            [Row.eD(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(0)], [Scl(1)], [Scl("-5")] ]
    ],
    size      = 8,
    )


zoo.add(
    name      = "Frank-Kamenetskii blow-up equation",
    slug      = "frankKamenetskiiBlowUp",
    type      = ["bvp"],
    domain    = "[-1,1]",
    equations = [ "u'' + 0.87 e^u = 0",
                  "u(1) = u(-1) = 0"],
    schema    = [
        [ [Blk("N : u \\mapsto u'' + 0.87 e^u", linear=False)],
            [Row.e(1)],
            [Row.e(-1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(0)] ]
    ],
    size      = 6,
    )


zoo.add(
    name      = "Interior layers (linear)",
    slug      = "interiorLayersLinear",
    type      = ["bvp"],
    domain    = "[-1,1]",
    equations = [ "\\varepsilon u'' + x (x^2-\\tfrac12) u' + 3 (x^2-\\tfrac12) u = 0",
                  "u(-1) = -2",
                  "u(1) = 4"],
    schema    = [
        [ [Blk("\\varepsilon D^2 + M_{x(x^2-0.5)} D \\\\\\quad + 3 M_{x^2-0.5}")],
            [Row.e(-1)], [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl("-2")], [Scl("4")] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Interior layers (nonlinear)",
    slug      = "interiorLayersNonlinear",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "\\varepsilon u'' + u u' = u",
                  "u(0) = -\\tfrac76",
                  "u(1) = \\tfrac32"],
    schema    = [
        [ [Blk("N : u \\mapsto \\varepsilon u'' + u u' - u", linear=False)],
            [Row.e(0)], [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl("\\tfrac{-7}6")], [Scl("\\tfrac32")] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Nonlinear pendulum",
    slug      = "pendulum",
    type      = ["bvp"],
    domain    = "[0,10]",
    equations = [ "u'' = -\\sin(u)",
                  "u(0) = 1",
                  "u(10) = 1"],
    schema    = [
        [ [Blk("N : u \\mapsto u'' + \\sin(u)", linear=False)],
            [Row.e(0)], [Row("\\mathbf{e}_{10}^T")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(1)], [Scl(1)] ]
    ],
    size      = 6,
    )


zoo.add(
    name      = "Piecewise forcing term",
    slug      = "piecewiseForcing",
    type      = ["bvp"],
    domain    = "[-5,5]",
    equations = [ "u'' - \\gamma u^3 = -(x\\geq0.9)(x\\leq1.1)",
                  "u(5) = 1",
                  "u'(-5) = 0"],
    schema    = [
        [ [Blk("N : u \\mapsto u'' - \\gamma u^3 \\\\\\quad + (x\\geq0.9)(x\\leq1.1)", linear=False)],
            [Row.e(5)], [Row.e(-5)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(1)], [Scl(0)] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Sawtooth problem",
    slug      = "sawtoothProblem",
    type      = ["bvp"],
    domain    = "[-1,1]",
    equations = [ "\\varepsilon u'' + (u')^2 = 1",
                  "u(-1) = \\alpha",
                  "u(1) = \\beta"],
    schema    = [
        [ [Blk("N : u \\mapsto u'' + (u')^2", linear=False)],
            [Row.e(-1)], [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(1)], [Scl("\\alpha")], [Scl("\\beta")] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "A system with coupled nonseparable BCs",
    slug      = "nonseparableBCs",
    type      = ["bvp"],
    domain    = "[-1,1]",
    equations = [ "u'' - v = x",
                  "x u + v'' = 0",
                  "u(-1) = v'(1)",
                  "2 u(1) = v'(-1)",
                  "u'(1) = 0",
                  "v(1) = 0"],
    schema    = [
        [ [Blk("D^2"), Blk("-I")],
            [Blk("M_x"), Blk("D^2")],
            [Row.e(-1), Row("-\\mathbf{e}_{1}^T D")],
            [Row("2\\mathbf{e}_{1}^T"), Row("-\\mathbf{e}_{-1}^T D")],
            [Row.eD(1), Row.bold(0)],
            [Row.bold(0), Row.e(1)] ],
        [ [Col.bold("u")], [Col.bold("v")] ],
        [ [Sym("=")] ],
        [ [Col("x")], [Col.bold(0)],
            [Scl(0)], [Scl(0)], [Scl(0)], [Scl(0)] ]
    ],
    size      = 12,
    )


zoo.add(
    name      = "Troesch equation",
    slug      = "troesch",
    type      = ["bvp"],
    domain    = "[0,1]",
    equations = [ "u'' = 8 \\sinh(8u)",
                  "u(0) = 0",
                  "u(1) = 1"],
    schema    = [
        [ [Blk("N : u \\mapsto u'' - 8 \\sinh(8u)", linear=False)],
            [Row.e(0)], [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(1)] ]
    ],
    size      = 7,
    )


zoo.add(
    name      = "Water droplet",
    slug      = "waterDroplet",
    type      = ["bvp"],
    domain    = "[-1,1]",
    equations = [ "u'' = (u-1)(1+(u')^2)^{3/2}",
                  "u(-1) = 0",
                  "u(1) = 0"],
    schema    = [
        [ [Blk("N : u \\mapsto \\\\\\quad u'' - (u-1)(1+(u')^2)^{3/2}", linear=False)],
            [Row.e(-1)], [Row.e(1)] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(0)] ]
    ],
    size      = 8,
    )



#-----------------------------------------------------------------------------
# Other examples
#-----------------------------------------------------------------------------

zoo.add(
    name        = "ODE-constrained minimization",
    date        = "29 April 2014",
    slug        = "odeMinCon",
    type        = ["opt"],
    domain      = "[0,1]",
    equations   = [ "\\min_u~~\\tfrac12 \\langle u,\,Au \\rangle - \\langle u,\,f \\rangle",
                    "\\text{s.t.}\\quad Bu = g,\\quad u(0) = 0"],
    schema      = [
        [
            [Blk("A"), Blk("B^*")],
            [Blk("B"), Blk(0)],
            [Row.e(0), Row.bold(0)]
        ],
        [
            [Col.bold("u")],
            [Col.bold("\\lambda")]
        ],
        [
            [Sym("=")]
        ],
        [
            [Col.bold("f")],
            [Col.bold("g")],
            [Scl("0")]
        ]
    ],
    size        = 7,
    )

# zoo.add(
#     name        = "Taut string under load",
#     date        = "27 June 2014",
#     slug        = "tautString",
#     type        = ["opt"],
#     domain      = "[0,1]",
#     equations   = [ "\\min_u \\tfrac12 \\langle Au,\, u \\rangle - \\langle f,\, u \\rangle",
#                     "\\text{s.t.}\quad Au = f,\quad u(0) = 0" ],
#     schema      = [
#         [
#             [Blk("I"), Blk("L^*")],
#             [Blk("L"), Blk(0)],
#             [Row.e(0), Row.bold(0)]
#         ],
#         [
#             [Col.bold("u")],
#             [Col.bold("\lambda")]
#         ],
#         [
#             [Sym("=")]
#         ],
#         [
#             [Col.bold(0)],
#             [Col.bold("f")],
#             [Scl("0")]
#         ]
#     ],
#     size        = 9,
#     refs        = [{"url": "",
#                     "text": "Stakgold, Ivar. <em>Green's functions and boundary value problems.</em> Vol. 99. John Wiley & Sons, 2011: p554."}]
#     )



zoo.add(
    name        = "Van der Pol limit cycle with unknown period",
    slug        = "vanDerPolUnknownPeriod",
    type        = ["other"],
    domain      = "[0,\\tau]",
    equations   = [ "u'' = (1-u^2)u' - u",
                    "u(0) = u(\\tau)",
                    "u'(0) = u'(\\tau)",
                    "u(0) = 1"],
    schema      = [
        [
            [Blk("N : (u,\\tau) \mapsto \\\\\\quad -u''+(1-u^2)u'-u", span=2, linear=False)],
            [Row("b_1 : (u,\\tau) \mapsto u(0) - u(\\tau)", span=2, linear=False)],
            [Row("b_2 : (u,\\tau) \mapsto u'(0) - u'(\\tau)", span=2, linear=False)],
            [Row.e(0), Scl(0)],
        ],
        [
            [Col.bold("u")], [Scl("\\tau")]
        ],
        [
            [Sym("=")]
        ],
        [
            [Col.bold(0)],
            [Scl("0")],
            [Scl("0")],
            [Scl("1")],
        ]
    ],
    size        = 9,
    )



zoo.add(
    hidden      = True,
    name        = "Integration by parts",
    slug        = "integrationByParts",
    type        = ["other"],
    domain      = "[0,1]",
    equations   = [ "\\int_0^1 u\\,dv = uv\\bigg|_0^1 - \\int_0^1 v\\,du",
                    "\\mathbf{u}^T (D \\mathbf{v}) = " \
                    + "(\\mathbf{e}_1^T \\mathbf{u})(\\mathbf{e}_1^T \\mathbf{v})" \
                    + " - (\\mathbf{e}_0^T \\mathbf{u})(\\mathbf{e}_0^T \\mathbf{v})" \
                    + " - \\mathbf{v}^T (D \\mathbf{u})",
                    ],
    # schema      = [
    #     [ [Row.bold("u")] ],   [ [Blk("D")] ],   [ [Col.bold("v")] ],   [ [Sym("=")] ],   [ [Sym("\\Bigg(")] ],   [ [Row.e(1)] ],   [ [Col.bold("u")] ],   [ [Sym("\\Bigg)\\Bigg(")] ],   [ [Row.e(1)] ],   [ [Col.bold("v")] ],   [ [Sym("\\Bigg)")] ],   [ [Sym("-")] ],   [ [Sym("\\Bigg(")] ],   [ [Row.e(0)] ],   [ [Col.bold("u")] ],   [ [Sym("\\Bigg)\\Bigg(")] ],   [ [Row.e(0)] ],   [ [Col.bold("v")] ],   [ [Sym("\\Bigg)")] ],   [ [Sym("-")] ],   [ [Row.bold("v")] ],   [ [Blk("D")] ],   [ [Col.bold("u")] ],
    # ],
    )


zoo.add(
    hidden      = True,
    name        = "Brachistochrone (1)",
    slug        = "brachistochrone",
    type        = ["other"],
    domain      = "[0,\\pi]",
    equations   = [ "u' u'' - \\tfrac14 u' (1 + (u')^2)^2 = 0",
                    "u(0) = 0",
                    "u(\\pi) = -2" ],
    text        = """
Deferring to other sources for an account of the 
<a href="http://www-history.mcs.st-andrews.ac.uk/HistTopics/Brachistochrone.html">rich history</a> 
of the brachistochrone problem, we instead begin with Johann Bernoulli's result that a solution 
$u(x)$ on $[0,\\pi]$ to the problem must satisfy the the equation 
\\begin{equation}
    u = \\frac{2g}{1+(u')^2}
\\end{equation}
with the constraints
\\begin{equation}
    u(0) = 0, \\qquad u(\\pi) = -2.
\\end{equation}
Differentiating both sides of (1) introduces an additional degree of freedom 
necessary to satisfy the two boundary conditions:
\\begin{equation}
    u' = -4g \\frac{u' u''}{\\Big( 1 + (u')^2 \\Big)^2}.
\\end{equation}
In accordance with standard convention we set the gravitational force 
to be negative, $g = -1$, and then with some final rearranging the equation reduces to  
\\begin{equation}
    u' u'' - \\frac14 u' \\Big( 1 + (u')^2 \\Big)^2 = 0.
\\end{equation}

See <a href='brachistochrone2.html'>Brachistochrone (2)</a> for an alternate version of this problem.
""",
    schema      = [
        [ [Blk("N : u \\mapsto \\\\\\quad u' u'' - \\tfrac14 u' (1 + (u')^2)^2", linear=False)],
            [Row.e(0)], [Row.e("\\pi")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(-2)] ]
    ],
    size        = 7,
    refs        = [{"url": "http://info.ifpan.edu.pl/firststep/aw-works/fsV/parnovsky/parnovsky.pdf",
                    "text": """Parnovsky, A. S. "Some generalisations of brachistochrone problem." Acta Physica Polonica-Series A General Physics 93 (1998): 55-64."""}]
    )


zoo.add(
    hidden      = True,
    name        = "Brachistochrone (2)",
    slug        = "brachistochrone2",
    type        = ["other"],
    domain      = "[0,2\\pi]",
    equations   = [ "u' u'' - \\tfrac14 u' (1 + (u')^2)^2 = 0",
                    "u(0) = u(2\\pi) = 0" ],
    text        = """
This is a second version of the brachistochrone problem
\\begin{equation}
    u' u'' - \\frac14 u' \\Big( 1 + (u')^2 \\Big)^2 = 0
\\end{equation}
wherein we compute the solution for the boundary conditions
\[ u(0) = u(2\\pi) = 0, \]
which turns out to be a full period of the cycloid traced out by a circle of 
radius 1.

The function $u(x) = 0$ solves the system as written, so we supply an initial 
condition $ u_{\\text{init}}(x) $ approximating the solution to ensure convergence. 
I'll note that if we cancel the $u'$ term in the governing equation, changing
\\begin{equation}
    u' = 4 \\frac{u' u''}{\\Big( 1 + (u')^2 \\Big)^2}
\\end{equation}
into
\\begin{equation}
    1 = 4 \\frac{u''}{\\Big( 1 + (u')^2 \\Big)^2},
\\end{equation}
then chebop still converges to the correct solution even with $ u_{\\text{init}}(x) = 0 $. 
But is the cancellation justified? It implies that $ u'' = \\tfrac14 $ at stationary points 
(which is in fact true for the cycloid).
""",
    schema      = [
        [ [Blk("N : u \\mapsto \\\\\\quad u' u'' - \\tfrac14 u' (1 + (u')^2)^2", linear=False)],
            [Row.e(0)], [Row.e("2\\pi")] ],
        [ [Col.bold("u")] ],
        [ [Sym("=")] ],
        [ [Col.bold(0)], [Scl(0)], [Scl(0)] ]
    ],
    size        = 7,
    refs        = [{"url": "http://info.ifpan.edu.pl/firststep/aw-works/fsV/parnovsky/parnovsky.pdf",
                    "text": """Parnovsky, A. S. "Some generalisations of brachistochrone problem." Acta Physica Polonica-Series A General Physics 93 (1998): 55-64."""}],
    notes       = """notes"""
    )


zoo.add(
    name        = "Integro-differential equation for traveling wave",
    slug        = "integroDifferentialWave",
    type        = ["other"],
    domain      = "[0,\\pi]",
    equations   = [ "(u(x)-c)u''(x)+(u'(x))^2-(Su)(x)=0",
                    "(Su)(x)=\\int_0^\\pi [\\cos(x-y)+\\cos(x+y)] u(x) dy",
                    "u(0) = c",
                    "(u'(0))^2 = (Su)(0)",
                    "u'(\\pi) = 0",
                    "\int_0^\pi u(x) dx = 0"],
    schema      = [
        [
            [Blk("N : (u,c) \mapsto \\\\\\qquad (u-c)u'' - (u')^2 - Su", span=2, linear=False)],
            [Row.e(0), Scl("-1")],
            [Row("B : (u,c) \mapsto \mathbf{e}_0^T [(u')^2-Su]", span=2, linear=False)],
            [Row.eD("\\pi"), Scl("0")],
            [Row.s(), Scl("0")],
        ],
        [
            [Col.bold("u")], [Scl("c")]
        ],
        [
            [Sym("=")]
        ],
        [
            [Col.bold(0)],
            [Scl("0")],
            [Scl("0")],
            [Scl("0")],
            [Scl("0")],
        ]
    ],
    size        = 9,
    refs        = [{"url": "http://www2.maths.ox.ac.uk/chebfun/publications/driscoll2010.pdf",
                    "text": """Driscoll, Tobin A. "Automatic spectral collocation for integral, integro-differential, and integrally reformulated differential equations." Journal of Computational Physics 229, no. 17 (2010): 5980-5998."""}],
    )



#-----------------------------------------------------------------------------
# Make.
#-----------------------------------------------------------------------------
if __name__ == "__main__":
    zoo.releaseTheKraken()
