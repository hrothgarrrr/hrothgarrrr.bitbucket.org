% Lotka-Volterra predator-prey

% Domain, operator, boundary conditions
dom = [0, 18];
x   = chebfun('x', dom);
op  = @(x,u,v) [ diff(u)/2 - u + u.*v
                 diff(v) + v - u.*v ];
bc  = @(x,u,v) [ u(0)-1
                 v(0)-2 ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
uu = N\f;
u = uu{1};
v = uu{2};

% Check the residuals
norm(op(x,u,v) - f)
bc(x,u,v)
