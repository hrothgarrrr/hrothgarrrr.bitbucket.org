% Schrodinger

% Domain, operator, boundary conditions
dom = [-3, 3];
x   = chebfun('x', dom);
h   = 0.1;
op  = @(x,u) -h^2*diff(u,2) + x.^2.*u;
bc  = @(x,u) [ u(-3)
               u(3) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A);
ev = quasimatrix(ev);

% Check the solution
norms(chebmatrix(A*ev - ew*ev))'
bc(x,ev)'
