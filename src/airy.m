% Airy equation

% Domain, operator, boundary conditions
dom = [-5, 5];
x   = chebfun('x', dom);
ep  = 0.01;
op  = @(x,u) ep*diff(u,2) - x.*u;
bc  = @(x,u) [ u(-5)
               u(5) ];
f   = 1;

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
