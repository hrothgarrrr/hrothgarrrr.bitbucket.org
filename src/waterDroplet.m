% Water droplet

% Domain, operator, boundary conditions
dom = [-1, 1];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,2) - (u-1).*(1+diff(u).^2).^(3/2);
bc  = @(x,u) [ u(-1)
               u(1) ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
