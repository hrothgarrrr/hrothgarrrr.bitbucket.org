% Piecewise forcing term

% Domain, operator, boundary conditions
dom = [-5, 5];
x   = chebfun('x', dom);
gam = 0.1;
op  = @(x,u) diff(u,2) - gam*u.^3;
bc  = @(x,u) [ feval(diff(u),-5)
               u(5)-1 ];
f   = -(x >= 0.9).*(x <= 1.1);

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
