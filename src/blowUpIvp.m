% Blow-up initial value problem

% Domain, operator, boundary conditions
dom = [0, 0.99];
x   = chebfun('x', dom);
op  = @(x,u) diff(u) - u.^2;
bc  = @(x,u) u(dom(1))-1;
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
