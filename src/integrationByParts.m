% Integration by parts

% Domain, identity, and primitive operator blocks
d = [0,1];
x = chebfun('x', d);
[Z, I, D, C, M] = linop.primitiveOperators(d);
[z, e, s, r] = linop.primitiveFunctionals(d);

% Two interesting functions to integrate by parts
u = log(2-x).*sin(20*x);
v = tanh(sin(5*x.^2)-.5);

% \integral(u dv)
u'*(D*v)

% u(1)v(1) - u(0)v(0) - \integral(v du)
(e(1)*u)*(e(1)*v) - (e(0)*u)*(e(0)*v) - v'*(D*u)

% The usual way to compute the same quantity in Chebfun.
sum(u.*diff(v))