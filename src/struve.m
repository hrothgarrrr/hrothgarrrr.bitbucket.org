% Struve's equation

% Domain, operator, boundary conditions
dom = [0, 12];
x   = chebfun('x', dom);
nu  = 0;
op  = @(x,u) diff(u,2) + diff(u)./x + (1-nu^2./x.^2).*u;
bc  = @(x,u) [ u(0)
               feval(diff(u),0)-1 ];
f   = (x/2).^(nu-1) / (sqrt(pi)*gamma(nu+1/2));

% Build the chebop
L = chebop(op, dom, bc, exp(-x.^2));

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
