% Interior layers (linear)

% Domain, operator, boundary conditions
dom = [-1, 1];
x   = chebfun('x', dom);
ep  = 0.0005;
op  = @(x,u) ep.*diff(u,2) + x.*(x.^2-0.5).*diff(u) + 3*(x.^2-0.5).*u;
bc  = @(x,u) [ u(-1)+2
               u(1)-4 ];
f   = 0;

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
