% Bacterial respiration

% Domain, operator, boundary conditions
dom = [0, 5];
x   = chebfun('x', dom);
a = 1.5;  b = 2;  gam = 0.1;
op  = @(x,u,v) [ diff(u) - b + u + u.*v./(1+gam*u.^2)
                 diff(v) - a     + u.*v./(1+gam*u.^2) ];
bc  = @(x,u,v) [ u(0)-2
                 v(0)-1 ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
uu = N\f;
u = uu{1};
v = uu{2};

% Check the residuals
norm(op(x,u,v) - f)
bc(x,u,v)
