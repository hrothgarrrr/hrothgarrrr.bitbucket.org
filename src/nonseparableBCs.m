% A system with coupled nonseparable BCs

% Domain, operator, boundary conditions
dom = [-1, 1];
x   = chebfun('x', dom);
op  = @(x,u,v) [ diff(u,2)-v
                 x.*u+diff(v,2) ];
bc  = @(x,u,v) [ u(-1)-feval(diff(v),1)
                 2*(u(1))-feval(diff(v),-1)
                 feval(diff(u),1)
                 v(1) ];
f   = [x; 0*x];

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
uu = L\f;
u = uu{1};
v = uu{2};

% Check the residuals
norm(op(x,u,v) - f)
bc(x,u,v)
