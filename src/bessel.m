% Bessel equation

% Domain, operator, boundary conditions
dom = [0, 30];
x   = chebfun('x', dom);
op  = @(x,u) x.^2.*diff(u,2) + x.*diff(u) + (x.^2-3^2).*u;
bc  = @(x,u) [ u(0)
               u(30)-1 ];
f   = 0;

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
