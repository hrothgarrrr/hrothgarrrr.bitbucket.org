% Sawtooth problem

% Domain, operator, boundary conditions
dom = [-1, 1];
x   = chebfun('x', dom);
ep  = 0.05;
op  = @(x,u) ep*diff(u,2) + diff(u).^2;
bc  = @(x,u) [ u(-1)-0.8
               u(1)-1.2 ];
f   = 1;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
