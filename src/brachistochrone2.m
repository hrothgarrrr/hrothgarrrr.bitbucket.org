% Brachistochrone problem (2)

% Domain, RHS, constants
dom = [0,2*pi];
b = 0;

% Build the operator
op = @(x,u) (diff(u,2) - .25*(1+diff(u).^2).^2).*diff(u);
bc = @(x,u) [u(0); u(2*pi)-b];
init = chebfun(@(x) 2*(x/pi-1).^2 - 2, dom);
N = chebop(op, dom, bc, init);

% Preferences
pref = cheboppref;
pref.maxIter = 3;
pref.discretization = @ultraS;
pref.plotting = 'off';
warning off

% Solve
sol = solvebvp(N, 0, pref);

% Check the solution
sol(0),  sol(2*pi)-b        % boundary conditions
sqrt(-sum(sol)/pi/3)        % radius of generating circle, should be 1
sum(sqrt(1+diff(sol).^2))   % arc length of solution, should be 8