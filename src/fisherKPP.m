% Fischer-KPP equation

% Domain, operator, boundary conditions
dom = [-4, 4];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,2) + u - u.^2;
bc  = @(x,u) [ u(dom(1))-1
               u(dom(2)) ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
