% Cantilevered beam

% Domain, operator, boundary conditions
dom = [0, 1];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,4);
bc  = @(x,u) [ u(0)
               feval(diff(u),0)
               feval(diff(u,2),1)
               feval(diff(u,3),1) ];
f   = -1;

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
