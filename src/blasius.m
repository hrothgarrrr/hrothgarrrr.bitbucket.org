% Blasius function
%
% Boyd, John P. "The Blasius function in the complex plane."
%   Experimental Mathematics 8, no. 4 (1999): 381-394.

% Domain, RHS, constants
dom = [0,6];

% Build the operator
op = @(x,u) 2*diff(u,3) + u.*diff(u,2);
bc = @(x,u) [u(0); feval(diff(u),0); feval(diff(u),dom(2))-1];
N = chebop(op, dom, bc);

% Preferences
pref = cheboppref;
pref.discretization = @ultraS;
pref.plotting = 'off';

% Solve
sol = solvebvp(N, 0, pref);

% Check the solution
sol(0),  feval(diff(sol),0),  feval(diff(sol),dom(2))-1
norm(2*diff(sol,3) + sol.*diff(sol,2))
feval(diff(sol,2),0),  0.33205733621519630