% Catenary problem

% Domain, RHS, constants
dom = [-1,1];
a = .5;
b = 1;
c = .5;

% Build the operator
% op = @(x,u) diff(u).^2 - u.^2/c^2 + a;
op = @(x,u) diff(u,2) - u/c^2;
bc = @(x,u) [u(-1)-a; u(1)-b];
N = chebop(op, dom, bc);

% Preferences
pref = cheboppref;
% pref.maxIter = 4;
% pref.discretization = @ultraS;
% pref.plotting = 'off';
warning off

% Solve
sol = solvebvp(N, 0, pref);
x = chebfun('x', dom);
% plot(sol, 'r', c*cosh(x/c), 'k--')


% Check the solution
% sol(0),  sol(pi)-b          % boundary conditions
% sqrt(-2*sum(sol)/pi/3)      % radius of generating circle, should be 1
% sum(sqrt(1+diff(sol).^2))   % arc length of solution, should be 4