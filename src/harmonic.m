% Harmonic oscillator

% Domain, operator, boundary conditions
dom = [-8, 8];
x   = chebfun('x', dom);
op  = @(x,u) -diff(u,2) + (x.^2).*u;
bc  = @(x,u) [ u(-8)
               u(8) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A);
ev = quasimatrix(ev);

% Check the solution
norms(chebmatrix(A*ev - ew*ev))'
bc(x,ev)'
