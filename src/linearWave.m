% Linear wave equation

% Domain, operator, boundary conditions
dom = [0, 10*pi];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,2) + u;
bc  = @(x,u) [ u(0)-2
               feval(diff(u),0) ];
f   = 0;

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
