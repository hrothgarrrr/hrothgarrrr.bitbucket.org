% System with variable coefficients

% Domain, operator, boundary conditions
dom = [-2, 2];
x   = chebfun('x', dom);
op  = @(x,u,v) [ diff(u,2) + x.*u + v
                 diff(v,2) + sin(x).*u ];
bc  = @(x,u,v) [ u(-2)
                 u(2)
                 v(-2)
                 v(2) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A,8);

% Check the solution
%norms(chebmatrix(A*ev - ew*ev))'
%bc(x,ev)'
