% Interior layers (nonlinear)

% Domain, operator, boundary conditions
dom = [0, 1];
x   = chebfun('x', dom);
ep  = 0.01;
op  = @(x,u) ep*diff(u,2) + u.*diff(u) - u;
bc  = @(x,u) [ u(0)+7/6
               u(1)-3/2 ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
