% Integro-differential equation for traveling wave

% Domain, operator, boundary conditions
dom = [0, pi];
x   = chebfun('x', dom);
K   = @(x,y) cos(x-y) + cos(x+y);
op  = @(x,u,c) diff(u,2).*(u-c) + diff(u).^2 - fred(K,u);
bc  = @(x,u,c) [ u(0)-c
                 feval(diff(u),0).^2 - feval(fred(K,u),0)
                 feval(diff(u),pi)
                 sum(u) ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u,c) - f)
bc(x,u,c)
