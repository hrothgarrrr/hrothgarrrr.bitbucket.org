% Coupled system with sin(v) and cos(u)

% Domain, operator, boundary conditions
dom = [-1, 1];
x   = chebfun('x', dom);
op  = @(x,u,v) [ diff(u,2)-sin(v)
                 diff(v,2)+cos(u) ];
bc  = @(x,u,v) [ u(-1)-1
                 feval(diff(u),1)
                 v(1)
                 feval(diff(u),-1) ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
uu = N\f;
u = uu{1};
v = uu{2};

% Check the residuals
norm(op(x,u,v) - f)
bc(x,u,v)
