% Nonlinear fourth-order equation

% Domain, operator, boundary conditions
dom = [0, 1];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,4) - diff(u).*diff(u,2) + u.*diff(u,3);
bc  = @(x,u) [ u(0)
               feval(diff(u),0)
               u(1)-1
               feval(diff(u),1)+5 ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
