% Van der Pol equation

% Domain, operator, boundary conditions
dom = [0, 15];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,2) - (1-u.^2).*diff(u) + u;
bc  = @(x,u) [ u(0)-2
               feval(diff(u),0) ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc, 2*cos(x));

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
