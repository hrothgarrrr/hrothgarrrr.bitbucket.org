% Harmonic oscillator as 1st-order system

% Domain, operator, boundary conditions
dom = [0, pi];
x   = chebfun('x', dom);
op  = @(x,u,v) [ -diff(v)
                  diff(u) ];
bc  = @(x,u,v) [ u(0)
                 u(pi) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A);

% Check the solution
%norms(chebmatrix(A*ev - ew*ev))'
%bc(x,ev)'
