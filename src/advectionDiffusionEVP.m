% Advection-diffusion (EVP)

% Domain, operator, boundary conditions
dom = [0, 10];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,2) + diff(u);
bc  = @(x,u) [ u(0)
               u(10) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A);
ev = quasimatrix(ev);

% Check the solution
norms(chebmatrix(A*ev - ew*ev))'
bc(x,ev)'
