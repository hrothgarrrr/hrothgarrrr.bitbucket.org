% Linear sawtooth

% Domain, operator, boundary conditions
dom = [0, 8];
x   = chebfun('x', dom);
op  = @(x,u) diff(u) + u/2;
bc  = @(x,u) u(0);
f   = sign(sin(x.^2));

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
