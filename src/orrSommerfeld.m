% Orr-Sommerfeld operator

% Domain, operator, boundary conditions
dom = [-1, 1];
x   = chebfun('x', dom);
gam = 5772;
opA = @(x,u) (diff(u,4)-2*diff(u,2)+u)/gam - 2i*u - 1i*(1-x.^2).*(diff(u,2)-u);
opB = @(x,u) diff(u,2) - u;
bc  = @(x,u) [ u(-1)
               u(1)
               feval(diff(u),-1)
               feval(diff(u),1) ];

% Build the chebop
A = chebop(opA, dom, bc);
B = chebop(opB, dom);

% Compute the eigenpairs
[ev, ew] = eigs(A,B,50);
ev = quasimatrix(ev);

% Check the solution
%norms(chebmatrix(A*ev - ew*ev))'
%bc(x,ev)'
