% Davies complex harmonic oscillator

% Domain, operator, boundary conditions
dom = [-8, 8];
x   = chebfun('x', dom);
op  = @(x,u) -diff(u,2) + (1i*x.^2).*u;
bc  = @(x,u) [ u(-8)
               u(8) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A, 7);
ev = quasimatrix(ev);

% Check the solution
norms(chebmatrix(A*ev - ew*ev))'
bc(x,ev)'
