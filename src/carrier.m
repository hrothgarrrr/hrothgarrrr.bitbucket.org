% Carrier equation

% Domain, operator, boundary conditions
dom = [-1, 1];
x   = chebfun('x', dom);
ep  = 0.01;
op  = @(x,u) ep*diff(u,2) + 2*(1-x.^2).*u + u.^2;
bc = @(x,u) [ u(-1)
              u(1) ];
f   = 1;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
