% Lane-Emden equation

% Domain, operator, boundary conditions
dom = [0, 10];
x   = chebfun('x', dom);
op  = @(x,u) x.*diff(u,2) + 2.*diff(u) + x.*u.^2;
bc  = @(x,u) [ u(0)-1
               feval(diff(u),0) ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
