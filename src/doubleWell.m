% Double well Schrodinger

% Domain, operator, boundary conditions
dom = [-5, 5];
x = chebfun('x', dom);
h = 0.1;
a = 4;  b = -1;  c = 0.9;
op = @(x,u) -h*diff(u,2) + a*(sign(x-b)-sign(x-c)).*u;
bc = @(x,u) [ u(-5)
              u(5) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A);
ev = quasimatrix(ev);

% Check the solution
norms(chebmatrix(A*ev - ew*ev))'
bc(x,ev)'
