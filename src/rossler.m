% Rossler attractor

% Domain, operator, boundary conditions
dom = [0, 10];
x   = chebfun('x', dom);
op  = @(x,u,v,w) [ diff(u) + v + w
                   diff(v) - u - 0.2*v
                   diff(w) - 0.2 - w.*(u-5.7) ];
bc  = @(x,u,v,w) [ u(0)-5
                   v(0)
                   w(0) ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
uu = N\f;
u = uu{1};
v = uu{2};
w = uu{3};

% Check the residuals
norm(op(x,u,v,w) - f)
bc(x,u,v,w)
