% Nonlinear pendulum

% Domain, operator, boundary conditions
dom = [0, 10];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,2) + sin(u);
bc  = @(x,u) [ u(0)-1
               u(10)-1 ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
u = N\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
