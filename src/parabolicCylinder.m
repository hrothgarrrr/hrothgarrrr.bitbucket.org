% Parabolic cylinder equation

% Domain, operator, boundary conditions
dom = [-8, 8];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,2) - (x.^2/4 - 3.5).*u;
bc  = @(x,u) [ feval(diff(u),0) + 1
               u(0) ];
f   = 0;

% Build the chebop
L = chebop(op, dom, bc, exp(-x.^2));

% Solve the differential equation
u = L\f;
plot(u)

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
