% Advection-diffusion equation with interior point conditions

% Domain, operator, boundary conditions
dom = [0, 1];
x   = chebfun('x', dom);
ep  = 0.02;
op  = @(x,u) ep*diff(u,2) + diff(u) + u;
bc  = @(x,u) [ feval(diff(u),0.5)-2
               sum(u.*(x<0.1)) ];
f   = 0;

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
