% Cross of eigenvalues

% Domain, RHS, constants
d = [0,2*pi];

% Primitive operator blocks
[Z, I, D, C, M] = linop.primitiveOperators(d);
[z, e, s, r] = linop.primitiveFunctionals(d);

% Build the operator
A = linop([ -D^2 ]);
B = linop([ D+I ]);
A = addbc( A, e(0), 0 );
A = addbc( A, e(2*pi), 0 );

% Solve
[EV, ew] = eigs(A,B,12);
sol = diag(ew)
