%-----------------------------------------------------------------------------
% Run all (or some) examples.
% Hrothgar, 27 Feb 2014
%-----------------------------------------------------------------------------

function RUN(varargin)

warning off MATLAB:dispatcher:nameConflict

if nargin > 0
    examples = varargin;
else
    examples = {};
    mfiles = dir('*.m');
    mfiles = {mfiles.name}';
    for k = 1:length(mfiles)
        redundant = mfiles{k};
        if ~strcmp(redundant, 'RUN.m')
            examples{end+1} = redundant(1:end-2);
        end
    end
end


outext = '.out';

for k = 1:length(examples)

    LW = 'linewidth'; lw = 2;
    FS = 'fontsize'; fs = 15;
    MS = 'markersize'; ms = 12;
    example = examples{k};

    try
        clear u
        clear uu
        clear ew
        output = evalc(example);

        % Plot the solution (if any).
        titletext = 'Solution';
        c = '';
        ylab = 'u(x)';
        if exist('uu') == 1
            ylab = 'u_n(x)';
            sol = real(uu);
        elseif exist('u') == 1
            c = 'k';
            sol = real(u);
        elseif exist('ev') == 1
            titletext = 'Eigenmodes';
            ylab = 'u_n(x)';
            sol = real(ev);
        end

        figure('position', [0,0,500,250])
        plot(sol, [c '-'], LW, lw, MS, ms)
        set(gcf, 'color', 'none'), box off
        title(titletext, FS, fs)
        xlabel('x', FS, fs), ylabel(ylab, FS, fs)
        export_fig('-transparent', '-painters', [example '.png'])
        close

        % Plot the eigenvalues (if any).
        if exist('ew') && ~isreal(ew)
            figure('position', [0,0,500,250])
            plot(ew, 'k.', LW, lw, MS, ms)
            set(gcf, 'color', 'none'), box off
            title('Eigenvalues', FS, fs)
            xlabel('x', FS, fs), ylabel('i y', FS, fs)
            export_fig('-transparent', '-painters', [example '-ew.png'])
            close
        end
    catch err
        warning(['Warning: ' example ' failed.'])
        output = [err.identifier ' :: ' err.message]
    end

    fh = fopen([example outext], 'w');
    fprintf(fh, output);
    fclose(fh);

end


return