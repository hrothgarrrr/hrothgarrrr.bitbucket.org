% Resonance

% Domain, operator, boundary conditions
dom = [0, 20];
x   = chebfun('x', dom);
op  = @(x,u) diff(u,2) + 25*u;
bc  = @(x,u) [ u(0)-1
               feval(diff(u),0) ];
f   = 10*cos(5*x);

% Build the chebop
L = chebop(op, dom, bc);

% Solve the differential equation
u = L\f;

% Check the residuals
norm(op(x,u) - f)
bc(x,u)
