% Lorenz equations

% Domain, operator, boundary conditions
dom = [0, 1];
x   = chebfun('x', dom);
op  = @(x,u,v,w) [ diff(u) - 10*(v-u)
                   diff(v) - u.*(3-w) + v
                   diff(w) - u.*v + 2*w ];
bc  = @(x,u,v,w) [ u(0)
                   v(0)-1
                   w(0)-1 ];
f   = 0;

% Build the chebop
N = chebop(op, dom, bc);

% Solve the differential equation
uu = N\f;
u = uu{1};
v = uu{2};
w = uu{3};

% Check the residuals
norm(op(x,u,v,w) - f)
bc(x,u,v,w)
