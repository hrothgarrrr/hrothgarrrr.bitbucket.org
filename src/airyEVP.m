% Airy operator (EVP)

% Domain, operator, boundary conditions
dom = [-1, 1];
x   = chebfun('x', dom);
ep  = 0.02;
op  = @(x,u) ep*diff(u,2) + x.*u;
bc  = @(x,u) [ u(-1)
               u(1) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A);
ev = quasimatrix(ev);

% Check the solution
norms(chebmatrix(A*ev - ew*ev))'
bc(x,ev)'

%%
% FIXME: This code currently breaks.