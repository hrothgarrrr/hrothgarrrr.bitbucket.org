% Derivative operator on periodic domain

% Domain, operator, boundary conditions
dom = [-pi, pi];
x   = chebfun('x', dom);
op  = @(x,u) -diff(u,2);
bc  = @(x,u) [ u(pi)-u(-pi)
               feval(diff(u),pi)-feval(diff(u),-pi) ];

% Build the chebop
A = chebop(op, dom, bc);

% Compute the eigenpairs
[ev, ew] = eigs(A);
ev = quasimatrix(ev);

% Check the solution
norms(chebmatrix(A*ev - ew*ev))'
bc(x,ev)'
