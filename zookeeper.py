import operator

#-----------------------------------------------------------------------------
# A couple of useful functions.
#-----------------------------------------------------------------------------

# A shortcut for "".join().
# (Well, it's only shorter by one character. But it's cleaner.)
def concat(lst): return "".join(lst)

# Grab the contents of a file.
# Return the empty string if the file doesn't exist.
def getFileContents(fileName):
    try:
        fh = open(fileName, 'r')
        contents = fh.read()
        fh.close()
        return contents
    except IOError:
        return False


#-----------------------------------------------------------------------------
# Helper classes for schema.
#-----------------------------------------------------------------------------
class AbstractOp:
    def __init__(self, text="", w=0, h=0, span=0, linear=True, isSymbol=False, padded=True):
        self.text = str(text)
        self.w = w
        self.h = h
        self.span = span or w or 1
        self.linear = linear
        self.isSymbol = isSymbol
        self.padded = padded
        self.width = self.height = None
        self.setCssClass()

    def setCssClass(self):
        if self.isSymbol:
            self.cssClass = 'Sym'
        else:
            self.cssClass = 'Blk' if (self.w and self.h) else \
                        'Col' if (not self.w and self.h) else \
                        'Row' if (self.w and not self.h) else 'Scl'
            if not self.linear:
                self.cssClass = self.cssClass.upper()

    def math(self):
        return " $ %s $ " % self.text

    def textIf(self, cssClass):
        return '$'+self.text+'$' if (self.cssClass == cssClass) else ''

    def tdAttributes(self, magicNumber):
        clas = 'class="%s"' % (self.cssClass)
        styl = 'style="width:%.0fpx;height:%.0fpx;"' % (magicNumber*self.width, magicNumber*self.height)
        cols = 'colspan="%i"' % (self.span) if (self.span != 1) else ''
        attr = " ".join(filter(None, [clas, styl, cols])) # The filter() removes empty strings.
        return attr

class Blk(AbstractOp):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('w', 1)
        kwargs.setdefault('h', 1)
        AbstractOp.__init__(self, *args, **kwargs)

class Col(AbstractOp):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('h', 1)
        AbstractOp.__init__(self, *args, **kwargs)

    @staticmethod
    def bold(s=''):
        return Col("\\mathbf{%s}" % s)

class Row(AbstractOp):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('w', 1)
        AbstractOp.__init__(self, *args, **kwargs)

    @staticmethod
    def e(s=''):
        return Row("\\mathbf{e}_{%s}^T" % s)

    @staticmethod
    def eD(s='', order=1):
        exp = '' if order == 1 else ('^%i' % order)
        return Row("\\mathbf{e}_{%s}^T D%s" % (s, exp))

    @staticmethod
    def s(s=''):
        return Row("\\mathbf{s}_{%s}^T" % s)

    @staticmethod
    def j(s=''):
        return Row("\\mathbf{j}_{%s}^T" % s)

    @staticmethod
    def jD(s='', order=1):
        exp = '' if order == 1 else ('^%i' % order)
        return Row("\\mathbf{j}_{%s}^T D%s" % (s, exp))

    @staticmethod
    def bold(s=''):
        return Row("\\mathbf{%s}^T" % s)

class Scl(AbstractOp):
    def __init__(self, *args, **kwargs):
        AbstractOp.__init__(self, *args, **kwargs)

class Sym(AbstractOp):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('isSymbol', True)
        kwargs.setdefault('h', 1)
        AbstractOp.__init__(self, *args, **kwargs)


# If any of the subblocks are unpadded, then the superblock is unpadded.
# This is really only used for the lambdas out in front of eigenvalue problems.
def isPadded(b):
    return sum(sum(d.padded for d in r) for r in b)


#-----------------------------------------------------------------------------
# The Example class.
#-----------------------------------------------------------------------------
class Example:
    """Block operator example."""
    def __init__(self, **properties):
        defaults = {
            "name": "empty example",
            "slug": "",
            "date": "",
            "type": [],
            "domain": "",
            "text": "",
            "equations": [],
            "description": "",
            "schema": [],
            "size": 0,
            "refs": [],
            "notes": "",
            "hidden": False,
        }
        self.__dict__.update(defaults)
        self.__dict__.update(properties)
        self.theSchemaHtml = ''
        self.theIconHtml = ''

    def __getitem__(self, key):
        return self.name

    def schemaHtml(self, magicNumber=25):
        if not self.theSchemaHtml and self.schema:
            self.reproportion()
            # I am so sorry, future me.
            self.theSchemaHtml = concat([ \
                '<div class="superblockwrap%s"><table class="superblock">' \
                    % ('' if isPadded(b) else ' unpadded') + \
                        concat([ '<tr>'+ \
                            concat([ '<td '+d.tdAttributes(magicNumber)+'>'+ \
                                d.math()+'</td>' for d in r ] \
                            )+'</tr>' for r in b ] \
                        )+'</table></div>' for b in self.schema ])
            self.theSchemaHtml = self.subSection("Schema",
                "<div class='schema'>%s</div>" % self.theSchemaHtml)
        return self.theSchemaHtml

    def iconHtml(self, magicNumber=3):
        if not self.theIconHtml:
            self.reproportion(12)
            self.theIconHtml = concat([ '<div class="iconblockwrap"><table class="iconblock">'+ \
                        concat([ '<tr>'+ \
                            concat([ '<td '+d.tdAttributes(magicNumber)+'>'+ \
                                d.textIf('Sym')+'</td>' for d in r ] \
                            )+'</tr>' for r in b ] \
                        )+'</table></div>' for b in self.schema ])
        return self.theIconHtml

    def reproportion(self, size=False):
        if size == False:
            size = self.size

        for (iu,unit) in enumerate(self.schema):
            numRowScls = sum([1 for r in unit if r[0].h==0])
            numRowBlks = sum([r[0].h for r in unit]) or -1
            blkHeight  = float(size-numRowScls) / numRowBlks
            for (ir,row) in enumerate(unit):
                numColScls = sum([1 for d in row if d.w==0])
                numColBlks = sum([d.w for d in row]) or -1
                blkWidth   = float(size-numColScls) / numColBlks
                for (id,d) in enumerate(row):
                    d.width  = blkWidth*d.w  or 1
                    d.height = blkHeight*d.h or 1
                    self.schema[iu][ir][id] = d

        return True

    @staticmethod
    def subSection(title, content, skip=True):
        skip = ' skip' if skip else ' noskip'
        html = """<div class='row{skip}'><div class='col col-md-3'><h3>{title}</h3></div>
                  <div class='col col-md-9 section'>{content}</div>
                  </div>""".format(title=title, content=content, skip=skip)
        return html

    def textHtml(self):
        if len(self.text) == 0:
            return ""
        else:
            # TODO: Implement real markdown here?
            txt = "<p class='description'>%s</p>" % self.text.replace("\n\n","</p><p class='description'>")
            txtHtml = self.subSection("Description", txt)
            return txtHtml

    def equationsHtml(self):
        if len(self.equations) == 0:
            return ""
        else:
            eqns = ['\\text{Domain:}\quad%s' % self.domain.replace(',',',\\ ')]
            eqns = eqns + self.equations
            eqns = concat(['\n\\[ %s \\]\n' % (eqn) for eqn in eqns])
            eqns = "<p class='equations'>%s</p>" % eqns
            eqnsHtml = self.subSection("Equations", eqns)
            return eqnsHtml

    def codeInputHtml(self):
        self.codeInput = getFileContents(self.codeInputUrl)
        if self.codeInput == False:
            return ""
        else:
            code = "<pre class='input prettyprint lang-matlab'>%s</pre>" % self.codeInput
            code = self.subSection("Code", code)
            return code

    def codeOutputHtml(self):
        self.codeOutput = getFileContents(self.codeOutputUrl)
        if self.codeOutput == False or self.codeOutput == "":
            return ""
        else:
            code = "<pre class='input prettyprint lang-matlab'>%s</pre>" % self.codeOutput
            code = self.subSection("Output", code, skip=False)
            return code

    def plotHtml(self):
        import os
        imgHtmls = []

        if os.path.exists(self.plotUrl):
            imgHtmls = imgHtmls + ['<img src="../%s" />' % self.plotUrl]

        if os.path.exists(self.plotEwUrl):
            imgHtmls = imgHtmls + ['<img src="../%s" />' % self.plotEwUrl]

        if len(imgHtmls) > 0:
            html = '<div class="plot">%s</div>' % "<hr/>".join(imgHtmls)
            html = self.subSection("Solution", html)
            return html
        else:
            return ""

    def refsHtml(self):
        if len(self.refs) == 0:
            return ""
        else:
            li = '<li><a href="{url}">{text}</a></li>'
            html = concat([li.format(**ref) for ref in self.refs])
            html = self.subSection("References", "<ol class='refs'>%s</ol>" % html)
            return html

    def templater(self):
        new = {
            "icon": self.iconHtml(),
            "text": self.textHtml(),
            "equations": self.equationsHtml(),
            "schema": self.schemaHtml(),
            "plot": self.plotHtml(),
            "codeInput": self.codeInputHtml(),
            "codeOutput": self.codeOutputHtml(),
            "refs": self.refsHtml(),
        }
        return dict(self.__dict__.items() + new.items())


#-----------------------------------------------------------------------------
# The Zoo class.
#-----------------------------------------------------------------------------
class Zoo:
    """Zoo of block operator examples."""
    def __init__(self):
        self.examples = []
        self.examplesDirectory = 'examples'
        self.codeDirectory = 'src'
        self.indexTemplate = 'template-index.html'
        self.exampleTemplate = 'template-example.html'

    def add(self, **properties):
        eg = Example(**properties)
        htmlPath = (self.examplesDirectory, eg.slug)
        codePath = (self.codeDirectory, eg.slug)
        eg.url           = '%s/%s.html' % htmlPath
        eg.plotUrl       = '%s/%s.png'  % codePath
        eg.plotEwUrl     = '%s/%s-ew.png' % codePath
        eg.codeInputUrl  = '%s/%s.m'    % codePath
        eg.codeOutputUrl = '%s/%s.out'  % codePath
        self.examples += [eg]

    def checkSlugs(self):
        import collections
        slugs = [eg.slug for eg in self.examples]
        empts = [eg.name for eg in self.examples if eg.slug == ""]
        dupes = [slug for slug, n in collections.Counter(slugs).items() if n > 1]
        if len(dupes) != 0:
            print("Error: Duplicate slug(s): %s." % str(dupes))
            raise Exception

        if len(empts) != 0:
            print("Error: Empty slug(s): %s." % str(empts))
            raise Exception

        return True

    def examplesOfType(self, s, includeHidden=False):
        if includeHidden:
            egg = [eg for eg in self.examples if s in eg.type]
        else:
            egg = [eg for eg in self.examples if (s in eg.type) and (eg.hidden==False)]

        return egg

    def indexHtml(self):
        types = {   "bvp": "Boundary value problems",
                    "ivp": "Initial value problems",
                    "evp": "Eigenvalue problems",
                    "pde": "Partial differential equations",
                    "opt": "Optimization",
                    "other": "Uncategorized" }

        sorted_types = sorted(types.iteritems(), key=operator.itemgetter(1))

        li = '<a href="%s" class="icon"><span>%s</span><br/>%s</a>'
        html = ''
        top = ' class="top"'

        for (key, val) in sorted_types:
            egg = self.examplesOfType(key)

            if len(egg) > 0:
                h2 = "<h2%s>%s<span class='count'>(%i)</span></h2>" % (top, val, len(egg))
                aa = concat([li % (eg.url, eg.name, eg.iconHtml()) for eg in egg])
                html = html + h2 + aa
                top = ''

        return html

    def releaseTheKraken(self):

        # Sort the examples.
        from operator import itemgetter
        self.examples = sorted(self.examples, key=itemgetter('name'))

        # Make sure none of the slugs are the same -- slugs are used for URLs.
        self.checkSlugs()

        # Get templates ready.
        indexTemplateHtml = getFileContents(self.indexTemplate)
        exampleTemplateHtml = getFileContents(self.exampleTemplate)

        # Loop through each and write a file.
        for eg in self.examples:

            # Generate HTML content.
            egDict = eg.templater()
            exampleHtml = exampleTemplateHtml.format(**egDict)

            # Write to file.
            fh = open(eg.url, 'w')
            fh.write(exampleHtml)
            fh.close()

        # Generate the index HTML.
        indexHtml = self.indexHtml()

        # Substitute with native templating, String.format().
        html = indexTemplateHtml.format(index=indexHtml)

        # Write it to the file.
        fh = open('index.html', 'w')
        fh.write(html)
        fh.close()
