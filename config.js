(function(jQuery){jQuery(document).ready(function() {

MathJax.Hub.Config({
    "HTML-CSS": { availableFonts: ["TeX"], scale: 106 },
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true,
        skipTags: ["script","noscript","style","textarea","pre","code"]
    },
    TeX: {
        Macros: {
            dif: ["\\frac{d{#1}}{d{#2}}", 2],
            pdif: ["\\frac{\\partial{#1}}{\\partial{#2}}", 2],
            Tr: "\\mathop{\\rm Tr}\\limits",
            real: "\\mathop{\\rm Re}\\nolimits",
            imag: "\\mathop{\\rm Im}\\nolimits",
            Res: "\\mathop{\\rm Res}\\limits",
            sign: "\\mathop{\\rm sgn}\\nolimits",
            i: "{\\rm i}"
        },
        equationNumbers: {autoNumber: "AMS"}
    }
});

prettyPrint();

});}(jQuery))